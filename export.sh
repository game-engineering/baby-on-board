#!/bin/bash
butler push exports/html5/ kaiec/baby-on-board:html5
butler push exports/win/baby-on-board-win.zip kaiec/baby-on-board:win
butler push exports/macos/baby-on-board-mac.zip kaiec/baby-on-board:macos
butler push exports/android/baby-on-board-android.apk kaiec/baby-on-board:android
butler push exports/linux-x64/baby-on-board-lin-x64.zip kaiec/baby-on-board:linux
