extends PanelContainer

signal value_changed(value)

@onready var label = $Label

@export var value : int = -1:
	set(new_val):
		if new_val != value:
			value = new_val
			value_changed.emit(value)
			if label:
				label.text = str(value)


func _ready():
	value = value


