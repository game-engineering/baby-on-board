extends Node

signal music_volume_changed(value)
signal sfx_volume_changed(value)

var player_name = ""

var music_volume = 0:
	set(value):
		if value != music_volume:
			music_volume = value
			music_volume_changed.emit(value)
		
var sfx_volume = 0:
	set(value):
		if value != sfx_volume:
			sfx_volume = value
			sfx_volume_changed.emit(value)
		
