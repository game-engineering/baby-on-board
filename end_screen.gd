extends ColorRect

@onready var line_edit = %LineEdit
@onready var achievement = %Achievement
@onready var download_button = %DownloadButton
@onready var score_counter = $"../ScoreCounter"
@onready var share = $Share
@onready var save_name_button = $VBoxContainer/HBoxContainer3/HBoxContainer/SaveNameButton

func _ready():
	if not OS.has_feature("web"):
		download_button.text = "Save Image"
	if OS.has_feature("android"):
		download_button.text = "Share Image"
		download_button.icon = preload("res://assets/share.svg")
	if OS.has_feature("web_android"):
		line_edit.focus_entered.connect(func(): save_name_button.show())
		line_edit.focus_exited.connect(func(): save_name_button.hide())
	line_edit.text = Globals.player_name
	line_edit.text_changed.connect(_on_line_edit_text_changed)
	line_edit.text_submitted.connect(_on_line_edit_text_changed)
	if len(str(line_edit.text).strip_edges()) > 0:
		download_button.show()

func _on_line_edit_text_changed(_new_text=""):
	achievement.player = line_edit.text
	Globals.player_name = line_edit.text
	if len(str(line_edit.text).strip_edges()) > 0:
		download_button.show()


func _on_new_round_button_pressed():
	get_tree().paused = false
	get_tree().reload_current_scene()

func _on_download_button_pressed():
	var date = Time.get_date_dict_from_system(true)
	var date_str =  "%d-%02d-%02d" % [date["year"], date["month"], date["day"]]
	var time_str = Time.get_time_string_from_system().replace(":", "")
	var file_name = "%s-baby-on-board-%s-%d.png" % [date_str, time_str, score_counter.value]
	var image = achievement.get_viewport().get_texture().get_image()
	if OS.has_feature("web"):
		JavaScriptBridge.download_buffer(image.save_png_to_buffer(), file_name, "image/png")
	elif OS.has_feature("android"):
		image.save_png(OS.get_user_data_dir() + "/" + file_name)
		share.share_image(OS.get_user_data_dir() + "/" + file_name, "Baby on Board!", "Achievement", "Want to beat me? Play at http://kaiec.itch.io/baby-on-board")
	else:
		image.save_png(OS.get_system_dir(OS.SYSTEM_DIR_PICTURES) + "/" + file_name)
		


func _on_back_to_menu_button_pressed():
	get_tree().paused = false
	get_tree().change_scene_to_file("res://scenes/menu/menu.tscn")
