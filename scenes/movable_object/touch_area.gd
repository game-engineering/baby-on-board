extends Area2D

signal swiped(direction)

var touched := false
var touch_index = -1
var touch_start_position = Vector2()

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.



func _input_event(_viewport, event, _shape_idx): 
	if event is InputEventScreenTouch and event.is_pressed() and not touched:
		touched = true
		touch_index = event.index
		touch_start_position = event.position
		
func _input(event):
	if touched and event is InputEventScreenDrag and event.index == touch_index and not event.is_pressed():
		var swipe_vector = event.position - touch_start_position
		if abs(swipe_vector.x) > 10:
			touched = false
			swiped.emit(signi(roundi(swipe_vector.x)))
