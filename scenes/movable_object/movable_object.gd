extends CharacterBody2D

signal collided(this, other)

@onready var sprite : RandomSprite = $Sprite
@onready var touch_area = $TouchArea
@onready var collision_shape_2d = $CollisionShape2D

enum Type {cars, sweets}
@export var type : Type

func _str(my_type : Type):
	return Type.keys()[my_type]

var lanes : Array[int] = [180, 344, 518, 676]
var current_lane : int = -1
var tween : Tween
var goto_position : Vector2
var crashed := false

static var recent : Array[int] = []

# Called when the node enters the scene tree for the first time.
func _ready():
	var selected = false
	while not selected:
		var index = sprite.set_random_texture(_str(type))
		if type == Type.cars:
			if index not in recent:
				# Prevent too many firefighters
				if index == 15 and randf() < 0.8:
					continue
				selected = true
				recent.append(index)
				while len(recent) > 10:
					recent.pop_front()
		elif type==Type.sweets:
			selected = true
	current_lane = randi_range(0, 3)
	position.x = lanes[current_lane]
	goto_position = position
	# print("Lane: ", current_lane, " Pos: ", position.x)
	touch_area.swiped.connect(func(direction): move(direction))
	var collision_rect = sprite.fit_collision_shape()
	collision_shape_2d.shape.size = collision_rect
	
	


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _physics_process(delta):
	var dir = Vector2(goto_position.x - position.x, velocity.y * delta)
	var collision = move_and_collide(dir)
	if collision and not crashed and not is_queued_for_deletion():
		# Can happen during spawning, just ignore
		if position.y < 0:
			queue_free()
			return
		crashed = true
		collided.emit(self, collision.get_collider())
		
	if position.y > 2100:
		queue_free()

#func _input(event):
	#if event.is_action_pressed("left"):
	#	_move_left()
	#elif event.is_action_pressed("right"):
	#	_move_right()
	

func move(direction):
	# print("Moving ", direction)
	move_to_lane(current_lane + direction)

func move_left():
	move_to_lane(current_lane - 1)

func move_right():
	move_to_lane(current_lane + 1)

func move_to_lane(lane : int):
	if tween:
		tween.kill()
	tween = create_tween()
	current_lane = clampi(lane, 0, len(lanes)-1)
	# Randomly add some value so that the cars do not form super-straight lines.
	tween.tween_property(self, "goto_position", Vector2(lanes[current_lane] + randf_range(-5, 5), position.y), 0.2)	
