class_name RandomSprite
extends Sprite2D

@export var sprite_frames : SpriteFrames


func set_random_texture(frame_set: String):
	var index = randi() % sprite_frames.get_frame_count(frame_set)
	texture = sprite_frames.get_frame_texture(frame_set, index)
	return index

func fit_collision_shape():
	var pos = get_rect().position
	var size = get_rect().size
	var l
	var r
	var t
	var b
	for x in range (pos.x, pos.x+size.x):
		l = x
		if is_pixel_opaque(Vector2(x, pos.y + size.y / 2)):
			break
	for x in range (pos.x + size.x - 1, pos.x, -1):
		r = x
		if is_pixel_opaque(Vector2(x, pos.y + size.y / 2 )):
			break
	for y in range (pos.y, pos.y + size.y):
		t = y
		if is_pixel_opaque(Vector2(pos.x + size.x / 2, y)):
			break
	for y in range (pos.y + size.y - 1, pos.y, -1):
		b = y
		if is_pixel_opaque(Vector2(pos.x + size.x / 2, y)):
			break
	return Vector2(r-l, b-t) * scale

