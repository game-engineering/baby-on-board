class_name Action
extends Node2D

signal timeout(direction)

@onready var label = $Label
@onready var timer : Timer = $Timer
@onready var left = $Left
@onready var right = $Right
@onready var beat : Beat = $Beat

@export var time := 1.0 :
	set(value):
		time = value
		if timer:
			timer.wait_time = value
@export var direction := -1

var drop_tween : Tween

func get_wait_time():
	if timer.is_stopped():
		return timer.wait_time
	else:
		return timer.time_left


func _ready():
	beat.hide()
	time = time
	if direction == 1:
		right.show()
		left.hide()
	else:
		left.show()
		right.hide()
	label.text = String.num(get_wait_time(), 1).pad_decimals(1)
	timer.timeout.connect(func(): 
		# print("Action finished: ", timer.wait_time)
		timeout.emit(direction)
		)

func start():
	if timer.is_stopped():
		# print("Action timer started: ", timer.wait_time)
		timer.start()
		beat.show()
		
func is_stopped():
	return timer.is_stopped()

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	label.text = String.num(get_wait_time(), 1).pad_decimals(1)
	var passed = (time - get_wait_time()) / time
	beat.beat = floor(passed * 4) + 1
	

func animate_drop(y : int, delay : float):
	if drop_tween:
		drop_tween.kill()
	drop_tween = create_tween().set_ease(Tween.EASE_OUT).set_trans(Tween.TRANS_BOUNCE)
	drop_tween.tween_interval(delay)
	drop_tween.tween_property(self, "position:y", y, 0.5)
