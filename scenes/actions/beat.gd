class_name Beat
extends HBoxContainer

@onready var beat_1 = $"Beat 1"
@onready var beat_2 = $"Beat 2"
@onready var beat_3 = $"Beat 3"
@onready var beat_4 = $"Beat 4"

@onready var beat_indicators : Array[CheckButton]= [beat_1, beat_2, beat_3, beat_4]

@export var beat := 1:
	set(value):
		beat = ((value - 1) % 4) + 1
		if beat_indicators:
			for i in range(1, 5):
				beat_indicators[i - 1].button_pressed = (beat == i)
		

# Called when the node enters the scene tree for the first time.
func _ready():
	beat = beat

