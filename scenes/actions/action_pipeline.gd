extends Node2D

signal timeout(direction)
signal waittime_changed(new_waittime)
signal started


const ACTION = preload("res://scenes/actions/action.tscn")
@onready var visible_actions = $VisibleActions

var actions : Array[Action] = []

@export var action_height := 200
@export var num_actions := 6
@export var min_time := 1.0
@export var max_time := 3.0
@export var wait_time := 2.0
@export var randomize_time := false
@export var result_range := [0, 3]
@export var start_result := 2

var projected_result : int = 0

func _ready():
	projected_result = start_result

func set_bpm(bpm: int):
	wait_time = 240.0 / bpm

func fill_pipeline():
	for i in range(num_actions + 1):
		add_action()


func add_action():
	var dir = 0
	if projected_result == result_range[1]:
		dir = -1
	if projected_result == result_range[0]:
		dir = 1
	if not dir:
		dir = [1, -1].pick_random()
	projected_result += dir
	var action = ACTION.instantiate()
	action.direction = dir
	if randomize_time:
		action.time = randf_range(min_time, max_time)
	else:
		action.time = wait_time
	actions.append(action)

func _on_action_timeout(direction):
	var first_child = visible_actions.get_child(0) as Action
	if visible_actions.get_child_count() > 1:
		var next_action = visible_actions.get_child(1) as Action
		next_action.start()
		next_action.timeout.connect(_on_action_timeout)
	timeout.emit(direction)
	first_child.queue_free()
	add_action()


func start():
	_update_visible_actions()
	var first_child = visible_actions.get_child(0) as Action
	if first_child.is_stopped():
		first_child.timeout.connect(_on_action_timeout)
		first_child.start()
		started.emit()

func _update_visible_actions():
	while visible_actions.get_child_count() < num_actions and len(actions) > 0:
		var action = actions.pop_front()
		visible_actions.add_child(action)
		action.position.y = 0
		for i in range(0, visible_actions.get_child_count()):
			var child = visible_actions.get_child(i) as Action
			child.animate_drop((visible_actions.get_child_count() - i)*action_height, 0.1*i)
		
func _physics_process(_delta):
	_update_visible_actions()
