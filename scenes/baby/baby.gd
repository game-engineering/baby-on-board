extends CharacterBody2D

signal baby_collided(car)
signal baby_collected(sweet)

var lanes : Array[int] = [180, 344, 518, 676]
var current_lane : int = 2
var tween : Tween
var goto_position : Vector2
var crashed := false
@onready var crash = $Crash

func _ready():
	goto_position = position
	crash.hide()

#func _input(event):
	#if event.is_action_pressed("left"):
	#	_move_left()
	#elif event.is_action_pressed("right"):
	#	_move_right()
		
func _physics_process(_delta):
	var collision = move_and_collide(goto_position - position)
	if collision and not crashed:
		if collision.get_collider().get_collision_layer()==4:
			# Sweet collected
			baby_collected.emit(collision.get_collider())
		else:
			_crash(collision.get_collider())
		

func _crash(collider: Object):
	crashed = true
	crash.scale = Vector2(0.1, 0.1)
	crash.show()
	crash.z_index = 10
	var crash_tween = create_tween()
	crash_tween.tween_property(crash, "scale", Vector2(2.5, 2.5), 0.3)
	crash_tween.tween_callback(func():
		baby_collided.emit(collider)
	)

func move(direction):
	move_to_lane(current_lane + direction)

func move_left():
	move_to_lane(current_lane - 1)

func move_right():
	move_to_lane(current_lane + 1)

func move_to_lane(lane : int):
	if tween:
		tween.kill()
	tween = create_tween()
	current_lane = clampi(lane, 0, len(lanes)-1)
	tween.tween_property(self, "goto_position", Vector2(lanes[current_lane], position.y), 0.2)	
