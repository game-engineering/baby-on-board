extends Sprite2D


@export var sprite : Sprite2D
@export var shadow_offset : Vector2 = Vector2(5, 5)
@export var shadow_scale : Vector2 = Vector2(1.1, 1.1)
@export var shadow_alpha := 0.4


func _ready():
	sprite.item_rect_changed.connect(_update_shadow)
	_update_shadow()

func _update_shadow():
	texture = sprite.texture
	region_enabled = sprite.region_enabled
	region_rect = sprite.region_rect
	scale = sprite.scale * shadow_scale
	position = sprite.position + shadow_offset
	modulate = Color(0, 0, 0, shadow_alpha)
