extends Node2D

@onready var credits_container = $CreditsContainer
@onready var options_container = $OptionsContainer
@onready var rules_container = $RulesContainer

@onready var path_follow_2d = $Path2D/PathFollow2D

@onready var music_slider = %MusicSlider
@onready var sfx_slider = %SFXSlider

@onready var audio_stream_player = $AudioStreamPlayer
@onready var engine_stream_player = $EngineStreamPlayer
@onready var menu_music_player = $MenuMusicPlayer
@onready var sweet_stream_player = $SweetStreamPlayer
@onready var crash_stream_player = $CrashStreamPlayer


# Called when the node enters the scene tree for the first time.
func _ready():
	path_follow_2d.progress = 540
	credits_container.hide()
	options_container.hide()
	rules_container.hide()
	var tween = create_tween().set_ease(Tween.EASE_OUT).set_trans(Tween.TRANS_CUBIC)
	tween.tween_property(path_follow_2d, "progress", 1000, 1)
	menu_music_player.play()



func _on_rich_text_label_meta_clicked(meta):
	OS.shell_open(meta)


func _on_close_credits_button_pressed():
	credits_container.hide()


func _on_credits_button_pressed():
	credits_container.show()


func _on_start_button_pressed():
	engine_stream_player.play()
	var tween = create_tween().set_ease(Tween.EASE_IN).set_trans(Tween.TRANS_CUBIC)
	tween.tween_property(path_follow_2d, "progress", 1020, 1).set_trans(Tween.TRANS_BOUNCE).set_ease(Tween.EASE_OUT)
	tween.tween_property(path_follow_2d, "progress", 2400, 1.5).set_trans(Tween.TRANS_CUBIC).set_ease(Tween.EASE_IN)
	tween.tween_callback(func():
		get_tree().change_scene_to_file("res://game.tscn")
		)


func _on_options_button_pressed():
	menu_music_player.stream_paused = true
	music_slider.value = Globals.music_volume
	sfx_slider.value = Globals.sfx_volume
	options_container.show()


func _on_close_options_button_pressed():
	audio_stream_player.stop()
	menu_music_player.stream_paused = false
	options_container.hide()


func _on_sfx_slider_value_changed(value):
	Globals.sfx_volume = value
	sweet_stream_player.volume_db = value
	crash_stream_player.volume_db = value


func _on_music_slider_value_changed(value):
	print(value)
	Globals.music_volume = value
	audio_stream_player.volume_db = value
	menu_music_player.volume_db = value


func _on_test_sfx_button_pressed():
	sweet_stream_player.play()
	sweet_stream_player.finished.connect(func(): crash_stream_player.play())


func _on_test_music_button_pressed():
	audio_stream_player.play()
	create_tween().tween_callback(func(): audio_stream_player.stop()).set_delay(5)


func _on_close_rules_button_pressed():
	rules_container.hide()


func _on_rules_button_pressed():
	rules_container.show()
