extends Sprite2D

signal finished

# Called when the node enters the scene tree for the first time.
func _ready():
	scale = Vector2(0.03, 0.03)
	z_index = 10
	var crash_tween = create_tween()
	crash_tween.tween_property(self, "scale", Vector2(0.875, 0.875), 0.3)
	crash_tween.tween_callback(func(): 
		finished.emit()
		queue_free()
	)
