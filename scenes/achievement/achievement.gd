extends Node2D

@onready var nameLabel = $Name
@onready var pointsLabel = $Points
@onready var dateLabel = $Date

@export var player := "John Doe":
	set(value):
		player = value
		if nameLabel:
			nameLabel.text = player
			update_date()

@export var points := 100:
	set(value):
		points = value
		if pointsLabel:
			pointsLabel.text = str(points)
			update_date()

func update_date():
	var date = Time.get_date_dict_from_system(true)
	if dateLabel:
		dateLabel.text = "%d-%02d-%02d" % [date["year"], date["month"], date["day"]]
	
func _ready():
	player = player
	points = points
