extends Node2D

const MOVABLE_OBJECT = preload("res://scenes/movable_object/movable_object.tscn")
const ACTION = preload("res://scenes/actions/action.tscn")
const CRASH = preload("res://scenes/crash/crash.tscn")

@onready var sweet_spawn_timer = $SweetSpawnTimer
@onready var obstacle_spawn_timer = $ObstacleSpawnTimer
@onready var spawn_position = $SpawnPosition
@onready var obstacles = $Obstacles
@onready var action_pipeline = $ActionPipeline
@onready var baby = $Baby
@onready var score_counter = $CanvasLayer/ScoreCounter
@onready var round_timer = $RoundTimer
@onready var end_screen = $CanvasLayer/EndScreen
@onready var achievement = %Achievement
@onready var audio_stream_player = $AudioStreamPlayer
@onready var sweets = $Sweets
@onready var sweet_stream_player = $SweetStreamPlayer
@onready var crash_stream_player = $CrashStreamPlayer



var time_begin : float
var time_delay : float
var started := false

func _ready():
	audio_stream_player.volume_db = Globals.music_volume
	sweet_stream_player.volume_db = Globals.sfx_volume
	crash_stream_player.volume_db = Globals.sfx_volume
	end_screen.hide()
	obstacle_spawn_timer.timeout.connect(_spawn_obstacle)
	sweet_spawn_timer.timeout.connect(_spawn_sweet)
	action_pipeline.set_bpm(90)
	action_pipeline.fill_pipeline()
	action_pipeline.timeout.connect(func(direction):
		# print("Baby moves! ", direction)
		baby.move(direction)
		)
	round_timer.start()
	time_begin = Time.get_ticks_usec()
	time_delay = AudioServer.get_time_to_next_mix() + AudioServer.get_output_latency()
	action_pipeline.started.connect(func(): 
		if not audio_stream_player.playing:
			audio_stream_player.play()
		)


func _process(_delta):
	if not started:
		var time = (Time.get_ticks_usec() - time_begin) / 1000000.0
		# Compensate for latency.
		time -= time_delay
		if time > -0.016:
			action_pipeline.start()
			started = true

func _spawn_obstacle():
	var mo = MOVABLE_OBJECT.instantiate()
	mo.type = mo.Type.cars
	mo.collision_layer = 2
	mo.collision_mask = 2
	mo.velocity.y = 300
	mo.collided.connect(_on_car_car_collided)
	mo.position = spawn_position.position
	obstacles.add_child(mo)
	obstacle_spawn_timer.wait_time = randf_range(0, 3)
	obstacle_spawn_timer.start()


func _spawn_sweet():
	var mo = MOVABLE_OBJECT.instantiate()
	mo.velocity.y = 600
	mo.type = mo.Type.sweets
	mo.collision_layer = 4
	mo.collision_mask = 4
	mo.position = spawn_position.position
	sweets.add_child(mo)
	sweet_spawn_timer.wait_time = randf_range(2, 5)
	sweet_spawn_timer.start()

func _on_round_timer_timeout():
	score_counter.value += 2
	if score_counter.value > 50 and sweet_spawn_timer.is_stopped():
		sweet_spawn_timer.start()


func _on_car_car_collided(car1, car2):
	crash_stream_player.play()
	score_counter.value -= 10
	var crash = CRASH.instantiate()
	car1.add_child(crash)
	crash.global_position = (car1.global_position + car2.global_position) / 2
	crash.finished.connect(func():
		if car1 and not car1.is_queued_for_deletion():
			car1.queue_free()
		if car2 and not car2.is_queued_for_deletion():
			car2.queue_free()
		)

func _on_baby_baby_collided(_car):
	crash_stream_player.play()
	achievement.points = score_counter.value
	achievement.player = Globals.player_name
	get_tree().paused = true
	var tween = create_tween()
	tween.tween_property(audio_stream_player, "volume_db", -50, 1)
	tween.tween_callback(func():
		end_screen.show()
		)





func _on_baby_baby_collected(sweet):
	sweet_stream_player.play()
	score_counter.value += 20
	sweet.queue_free()
